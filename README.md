Snel Starten met HAMNET
=======================

Benodigdheden:
- Internet verbinding (die je gebruikt om dit te lezen)
- Een HamNet IP adres
- Een [MikroTik wifi product](https://routerboard.com/), zoals bijvoorbeeld de [SXT SA5](https://routerboard.com/RBSXTG-5HPnD-SAr2)

Adres aanvragen
---------------

Op het HAMNET heeft iedereen zijn eigen IP adres(sen). Deze adressen worden handmatig beheerd omdat het hier om publieke internet adressen gaat die starten met het getal "44". In Nederland gebruiken we de adressen 44.137.x.x. Alle in Nederland uitgedeelde adressen staan in de [Nederlandse hosts file](http://pe1chl.nl.eu.org/hosts/hosts.137). In die file staat ook uitgelegd hoe je zo'n adres aanvraagt.

Een HAMNET access-point vinden
------------------------------

De [doelstelling van HAMNET](https://www.hamnet.nl/doelstelling/) is een autonoom netwerk voor de radioamateur. Dus is het de bedoeling om zoveel mogelijk via radio met HAMNET te verbinden. Daarvoor heb je een access-point bij jou in de buurt nodig.

De locatie van alle HAMNET access-points in Nederland en Duitsland kun je op de [kaart van hamnetdb.net](http://hamnetdb.net/lsp_map.cgi) vinden. Met een beetje moeite kun je van sommige access points zelfs de dekkingsplot zien:

![Dekkings plot](img/hamnetdb-pi1nos-dekking.png)

Deze beschrijving gaat er vanuit dat je wilt aansluiten op een van de access-points in de zendmast van Hilversum (natuurlijk omdat dat ook is wat ik deed).

Als je een access point hebt geselecteerd, dan heb je waarschijnlijk nog meer informatie nodig, zoals het wachtwoord (de WPA2 Pre-Shared Key). Als de hamnetdb kaart niet voldoende informatie geeft, neem dan contact op met de [HAMNET Initiatiefnemers](https://www.hamnet.nl/de-initiatiefnemers/) of vul het [contactformulier op hamnet.nl](https://www.hamnet.nl/contact/) in met je vraag.

Aan de slag met de MikroTik
---------------------------

Alle MikroTik producten gebruiken het RouterOS, en de handleiding daarvoor staat op de
[MikroTik wiki pagina](http://wiki.mikrotik.com/wiki/Manual:TOC).

Sluit de MikroTik niet aan op je thuisnetwerk, dat bemoeilijkt de zaak. Behandel de HAMNET "aansluiting" in eerste instantie als een nieuwe, aparte, internetaansluiting.

Sluit een computer aan op de MikroTik. Als beide apparaten zijn opgestart zal je computer een adres hebben gekregen van de MikroTik DHCP server. Open een browser en ga naar de MikroTik configuratie pagina op [http://192.168.88.1/](http://192.168.88.1/).

Als eerste zetten we het ip adres waarmee de MikroTik zich op het HAMNET meldt op het adres wat we hebben aangevraagd. Dit is belangrijk om het netwerkverkeer op HAMNET niet te ontregelen. Klik daarvoor op "Quick Set".

<a href="img/mikrotik-quick-set.png"><img width="100%" src="img/mikrotik-quick-set.png"/></a>

Vul bij "Wireless Network" aan de rechter kant van het scherm de volgende waarden in:

- IP Address: *het HAMNET adres dat je hebt ontvangen*
- Netmask: 255.255.252.0 (/22)
- Gateway: 44.137.36.254
- DNS Servers: 44.137.0.1

In Hilversum en omstreken wordt over het algemeen een 10MHz bandbreedte gebruikt voor HAMNET. Dat betekent dat je met de standaard instellingen van een WiFi router de HAMNET access-points niet zult zien. In de MikroTik verander je die instelling door links in het menu op "Wireless" te klikken, en vervolgens op de regel die begin met "wlan1". Je ziet dan het volgende scherm:

<a href="img/mikrotik-10Mhz-setting.png"><img width="100%" src="img/mikrotik-10Mhz-setting.png"/></a>

Vul de volgende waarden in:

- Mode: station
- Band: 5GHz-A/N
- Channel width: 10MHz
- Wireless protocol: 802.11

Om de MikroTik te voorzien van de juiste landinstellingen, klik je op "Advanced mode" bovenaan het scherm. Vervolgens scroll je naar beneden en kom je de volgende settings tegen:

<a href="img/mikrotik-region-settings.png"><img width="100%" src="img/mikrotik-region-settings.png"/></a>

Zorg ervoor dat de waarden van de aangegeven velden als volgt zijn ingevuld:

- Frequency Mode: manual-txpower
- Country: netherlands
- Antenna Gain: *optioneel, selecteer de gain van je antenne*

Het invullen van de Antenne Gain zorgt ervoor dat de gebruikersinterface de juiste waarden weergeeft. Als je de antenne gain niet precies weet, kun je ook gewoon 0 invullen.

De overige waarden laten we staan zoals ze stonden.

Richt de MikroTik op het dichtstbijzijnde access-point. In mijn geval was het dichtstbijzijnde access-point 14km verderop, dus een beetje geduld is hier wel noodzakelijk. 

Klik weer op het "Wireless" menu, en vervolgens op de knop "Scanner" bovenaan het scherm. In het volgende scherm klik je op "Start". Na een paar seconden zou het access-point waar je op richt in de lijst moeten verschijnen. Als dat niet het geval is kun je de volgende dingen controleren:

- Richt je wel op het juiste access-point, in de juiste richting?
- Komt de bandbreedte wel overeen met het access-point dat je probeert te vinden? De beheerder
  van het access-point zou bijvoorbeeld ook kunnen hebben gekozen om niet de 10MHz
  bandbreedte aan te houden.
  
Klik in de lijst met gevonden access points op het access-point waarmee je wilt connecten. Er verschijnt een overzicht met de details van het access-point. Bovenaan zit een "Connect" knop. Klik daarop.

De "Connect" knop zorgt ervoor dat de wlan1 interface het SSID en de frequentie van het access-point overneemt. Maar er komt nog geen verbinding tot stand. Dat komt doordat we het wachtwoord nog niet hebben ingevuld. Daarvoor moeten we een security profile aanmaken.

Klik op "Wireless" en vervolgens op de tab "Security Profiles" bovenaan de pagina. Er staat een "default" profile, dat laten we voor wat het is. Klik op de "Add new" knop. Je ziet nu het volgende scherm:

<a href="img/mikrotik-security-profile.png"><img width="100%" src="img/mikrotik-security-profile.png"/></a>

Vul de volgende waarden in:

- Mode: dynamic keys
- Authentication Types: WPA2 PSK
- WPA2 Pre-Shared Key: *het WiFi wachtwoord dat je hebt ontvangen van de access-point beheerder*

Klik op "Ok".

De laatste stap is het selecteren van deze security profile in de wlan1 interface. Klik op "Wireless" en dan op de "wlan1" regel. Bij "Security Profile" zou je nu het zojuist aangemaakte security profile moeten kunnen kiezen. Doe dat, en klik op "Ok".

Als het goed is zou de MikroTik nu na een paar seconden automatisch verbinding moeten maken met het geselecteerde draadloze netwerk. Links in het menu is er een optie "Log". Daarin zou je een regel moeten kunnen terugvinden met de tekst "Established Connection".

Test de verbinding door een hamnet pagina te openen. Op [sys3.pa3pm.ampr.org](http://sys3.pa3pm.ampr.org/) staan wat interessante links en tips. Deze server is ook te gebruiken om te pingen bij het testen van de verbinding.

Indien de MikroTik geen verbinding maakt met het netwerk, zal dat _niet_ worden vermeld in het log. Het is nog onduidelijk waar/hoe de MikroTik laat weten wat de problemen zijn als er geen verbinding gemaakt kan worden. Als er geen verbinding word gemaakt, loop dan alle instellingen grondig na.

Beter uitrichten
----------------

Om de snelheid van de verbinding te optimaliseren, kun je proberen de MikroTik beter uit te richten. Daarvoor is een snelle S-meter wel handig. Die is er niet, maar er is wel een grafiekje dat relatief snel bijgewerkt wordt. Klik op "Quick Set" in het menu. Gebruik het grafiekje op die pagina om de MikroTik (voorzichtig, langzaam) uit te richten.

<a href="img/mikrotik-wireless-signal.png"><img width="100%" src="img/mikrotik-wireless-signal.png"/></a>

Help! (MikroTik Factory Reset)
------------------------------

Het kan voorkomen dat bij het wijzigen van netwerk instellingen de MikroTik niet meer reageert of niet meer op het ip adres luistert dat je verwacht. In die situatie is het misschien het verstandigst om de MikroTik terug te brengen naar de fabrieksinstellingen. Dat kan als volgt:

1. Maak de Mikrotik spanningsloos.
2. Houdt de reset knop ingedrukt
3. Met de reset knop nog ingedrukt, sluit de spanning weer aan.
4. Zodra het "USR" lampje begint te knipperen, laat je de reset knop los.
5. Wacht een minuutje totdat de MikroTik zijn settings heeft teruggezet en is herstart.
6. Open een webbrowser op [http://192.168.88.1/](http://192.168.88.1/) om te zien of het is gelukt.
