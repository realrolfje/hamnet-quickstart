Hamnet Chat
===========

Zendamateurs zijn eigenwijs. Het moet beter, anders gekker, groter, kleiner, sneller
of trager dan "de rest van de wereld". Maar vooral: We willen het zelf doen.

Een wereldwijd DMR communicatie netwerk, waarvan het netwerk verkeer ook nog eens
over een zelf ingericht 44.x.x.x internet segment (Hamnet) werkt. Dan is het eigenlijk
vreemd dat we niet ook zelf onze "Appjes" transporteren en verwerken. Daarom is er door
[Stichting Scoop Hobbyfonds](https://www.hobbyscoop.nl/onze-stichting/) een Mattermost
server ingericht op Hamnet.

Hier hebben we een eigen plek, bereikbaar vanaf de smartphone en PC, die we zelf kunnen
inrichten en gebruiken. Bijvoorbeeld voor het afspreken van een tijd en frequentie, het
aankondigen van een rondje of het organiseren van een contest. Een berichtje sturen naar
een call? Dat kan nu dus.

<img style="float: right" width="150px" align="right" src="mattermost-dorpsplein.png"></img>
[Mattermost](https://github.com/mattermost) is een Open Source
[MIT license](https://en.wikipedia.org/wiki/MIT_License) groeps-chat applicatie die
gericht is op grote project teams en/of bedrijven. Mensen die al bekend zijn met Slack of
HipChat zullen het meteen herkennen: Publieke kanalen of "rooms" waarin meerdere mensen
met elkaar over een onderwerp van gedachten wisselen. Ook privé berichten en groepsgesprekken
behoren tot de mogelijkheden, voor als dingen even onder vier ogen moeten.

Mattermost bewaart alle historie, is doorzoekbaar, en voorzien van alle moderne gemakken
zoals desktop applicaties, smartphone "Apps" met indien gewenst  bijbehorende push- en
email notificaties. Het is snel en eenvoudig in gebruik, ondersteunt opgemaakte tekst,
emoji, en het het delen van bestanden en plaatjes.

![Berichten per dag](posts.png)
De afgelopen paar maanden is de Mattermost server door een groep van 20 amateurs gebruikt,
onder andere voor de werkzaamheden aan
[Hamnet en PI6ATV in IJsselstein](https://www.hobbyscoop.nl/2017/hamnet-en-pi6atv-in-ijsselstein/).
De configuratie is gladgestreken, de server is voorzien van nette HTTPS certificaten,
en inmiddels zijn er ook koppelingen met andere systemen gemaakt waardoor servers
via Mattermost aan de beheerders laten weten wat er aan de hand is.

Maar er word natuurlijk ook gewoon gekletst over radio's.

Alle verkeer van/naar Mattermost loopt via HTTPS, en data wordt lokaal opgeslagen op een
manier die niet eenvoudig te benaderen is, zelfs niet voor beheerders. Geen reclame,
geen winstoogmerk, geen filtering, geen profiling.

Hoewel de chatserver om praktische en technische redenen ook vanaf het "normale" internet
bereikbaar is, is deze chatserver uitsluitend bedoeld en toegankelijk voor geregistreerde
radiozendamateurs.

Meedoen? Hoe dat moet lees je [hier](https://mattermost.pi9noz.ampr.org/info/help.html).
